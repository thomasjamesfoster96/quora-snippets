### Orphaned Quora links. 

Quora has some feeds and pages that aren't accessible from the user interface. Some of them are included below.

*   [Directory - Quora](http://www.quora.com/directory/)

    Quora has a directory of every user, topic, question and blog on the site. This is never shown as a link on the site anywhere, but for automated processes it can be quite useful. It appears to be used for SEO purposes.
    
*   [Reviews Feed - Quora](http://www.quora.com/reviews)

    The latest redesign to the feed remove the link to the Reviews feed. Here it is.
    
*   [Quora Shuffle](http://www.quora.com/shuffle)
    
    Quora's little known Shuffle feature lets you browse random content. 
